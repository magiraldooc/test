<?php

namespace App\Http\Controllers;

use App\Mail\TestMail;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index(Request $request){
        return view('welcome');
    }

    public function processFile(Request $request){

        $customMessages = [
            'email.required' => 'Correo requerido',
            'csvFile.required' => 'Archivo CSV requerido',
            'csvFile.mimes' => 'Solo se admite formato csv',
            'csvFile.max' => 'Peso máximo 2MB',
        ];

        $request->validate([
            'email' => 'required',
            'csvFile' => 'required|mimes:csv,txt|max:2048'
        ], $customMessages);
        $email = $request->get('email');

        $handle = fopen($request->file('csvFile'), "r");
        //$handle = fopen($request->file('csvFile')->getPathname(), "r");

        // = fopen($s3File, "r");
        $header = true;
        $lineCount = 0;
        $errors = [];
        $resultFile = [['cedula','cedula_hash','nombre']];

        while ($csvLine = fgetcsv($handle, 1000, ",")) {
            $lineCount ++;
            if(count($csvLine) != 2){
                array_push($errors, 'En la linea '.$lineCount.' se esperan dos valores, se encontraron '.count($csvLine));
            } else {
                if ($header) {
                    if ($csvLine[0] != 'cedula' || $csvLine[1] != 'nombre'){
                        array_push($errors, 'Los encabezados del archivo son incorrectos, deben ser cedula,nombre');
                    }
                    $header = false;
                } else {
                    if(!is_numeric($csvLine[0])){
                        array_push($errors, 'En la linea '.$lineCount.' el primer valor debe ser solo números');
                    }
                    array_push($resultFile, [$csvLine[0],hash('sha256', $csvLine[0]),str_replace('"', '', $csvLine[1])]);
                }
            }
        }

        if ($errors && count($errors) > 0){
            return redirect()->back()->withErrors($errors);
        }else{
            $s3Path = $request->file('csvFile')->store('', 's3');
            //dd($s3Path);

            $s3File = Storage::disk('s3')->get($s3Path);
            $tempPath = Storage::disk('public');
            $tempPath->put($s3Path, $s3File);
            //$storagePath  = Storage::disk('public')->getDriver()->getAdapter()->getPathPrefix();
            $tempPathHash = Storage::disk('public');
            $tempPathHash->put('hash_'.$s3Path, '');
            $storagePathHash  = Storage::disk('public')->getDriver()->getAdapter()->getPathPrefix();
            $handleHash = fopen($storagePathHash.'hash_'.$s3Path, "wr");

            foreach ($resultFile as $row) {
                fputs($handleHash, implode(',', $row)."\n");
            }

            fclose($handleHash);
            $s3 = Storage::disk('s3');
            $s3->put('hash_'.$s3Path, fopen($storagePathHash.'hash_'.$s3Path, "r+"));
            $client = $s3->getDriver()->getAdapter()->getClient();
            $expiry = "+20 minutes";
            $command = $client->getCommand('GetObject', [
                'Bucket' => Config::get('filesystems.disks.s3.bucket'),
                'Key'    => 'hash_'.$s3Path
            ]);

            $request = $client->createPresignedRequest($command, $expiry);

            $s3FileUrl = (string) $request->getUri();

            Storage::disk('public')->delete($s3Path);
            Storage::disk('public')->delete('hash_'.$s3Path);
            Mail::to([$email])->send(new TestMail($s3FileUrl));
        }

        return redirect()->back()->with('success', 'Archivo enviado');
    }
}
