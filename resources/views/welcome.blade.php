<html>
<head>
    <title>Formulario</title>
    <style>
        form {
            max-width: 600px;
            margin: 0 auto;
            font-family: 'Open Sans', sans-serif;
            font-weight: weight;
            font-style: normal;
            padding: 10px;
        }
        form fieldset {
            border: none;
            font-weight: normal;
        }
        form input[type="file"],
        form input[type="email"],
        form textarea {
            box-sizing: border-box;
            outline: none;
            display: block;
            color: #333;
            width: 100%;
            padding: 7px;
            border: none;
            border-bottom: 1px solid #ddd;
            margin-bottom: 10px;
            font-family: inherit;
            font-size: 18px;
            height: 45px;
        }
        form textarea {
            height: 200px;
        }
        form button {
            display: block;
            padding: 19px 39px 18px 39px;
            color: #fff;
            background: #1abc9c;
            font-size: 18px;
            width: 100%;
            border: 1px solid #16a085;
            border-width: 1px 1px 3px;
            margin-top: 50px;
            margin-bottom: 10px;
            cursor: pointer;
            transition: all 0.3s;
            outline: none;
        }
        form button[type="submit"]:hover {
            background: #109177;
        }
    </style>
</head>
<body>

<form action="{{route('process')}}" enctype="multipart/form-data" method="post">
    @csrf
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if (Session::get('success'))
        <div class="success">
            {{Session::get('success')}}
        </div>
    @endif
    <h1>Cifrar</h1>
    <fieldset>
        <input type="hidden" name="_language" value="pt" />

        <input type="email" name="email" placeholder="Correo">

        <input name="csvFile" type="file" placeholder="CSV"></input>

        <button>Procesar</button>
    </fieldset>
    <!--<div>
        <label for="email"></label>
        <input type="email" name="email" required>
    </div>
    <div>
        <label for="csvFile"></label>
        <input type="file" name="csvFile">
    </div>
    <div>
        <button>Procesar</button>
    </div>-->
</form>
</body>
</html>
